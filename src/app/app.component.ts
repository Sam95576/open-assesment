import { DOCUMENT } from '@angular/common';
import { Component, HostListener, Inject, OnInit } from '@angular/core';
import { Options } from '@angular-slider/ngx-slider';
// import {  } from "../assets/images/";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  public monthlyIncome: any = 0;
  public monthlyExpense: any = 0;
  value1: number = 0;
  value2: number = 100;
  options: Options = {
    floor: 100000,
    ceil: 300000
  };

  options2: Options = {
    floor: 100000,
    ceil: 300000
  };


  constructor(@Inject(DOCUMENT) private document: Document) {

  }

  @HostListener('window:scroll', [])
  onWindowScroll() {
    if (
      document.documentElement.scrollTop > 180) {
      document.getElementById('navbar').classList.add('bg-default');
    }
    if (
      document.documentElement.scrollTop < 180) {
      document.getElementById('navbar').classList.remove('bg-default');
    }
  }


  ngOnInit() {
  }

  sliderEvent(value) {
    this.monthlyIncome = value.value
  }
  expenseChange(value) {
    this.monthlyExpense = value.value
  }
}
